package com.fast_development_corp.vaporwavewallpers;


import android.app.Application;

import com.fast_development_corp.vaporwavewallpers.utils.FileUtils;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        initFileStuff();
    }

    private void initFileStuff() {
        FileUtils.prepareDirectory();
    }

}
