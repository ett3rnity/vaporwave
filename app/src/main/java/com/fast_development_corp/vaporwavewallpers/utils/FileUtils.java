package com.fast_development_corp.vaporwavewallpers.utils;


import android.os.Environment;
import android.util.Log;

import com.fast_development_corp.vaporwavewallpers.BuildConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class FileUtils {
    public static final String FILE_SEPARATOR = "/";
    public static final String DESTINATION_FOLDER = "vaporwave_wallpapers";
    public static final String VAPORWAVE_PREFIX = "VAPORWAVE_";
    public static final String JPEG_POSTFIX = ".jpeg";


    public static boolean prepareDirectory() {
        File destinationDirectory = new File(Environment.getExternalStorageDirectory()
                + FILE_SEPARATOR + DESTINATION_FOLDER);
        if (!destinationDirectory.exists() && !destinationDirectory.isDirectory()) {
            if (destinationDirectory.mkdirs()) {
                if (BuildConfig.DEBUG) Log.i("CreateDir", "App dir created");
                return true;
            } else {
                if (BuildConfig.DEBUG) Log.w("CreateDir", "Unable to create app dir!");
                return false;
            }
        } else {
            if (BuildConfig.DEBUG) Log.i("CreateDir", "App dir already exists");
            return true;
        }
    }

    public static void saveImageFile(String fileName, File resource) {
        File destination = new File(Environment.getExternalStorageDirectory()
                + FILE_SEPARATOR + DESTINATION_FOLDER + FILE_SEPARATOR, VAPORWAVE_PREFIX + fileName + JPEG_POSTFIX);

        try {
            copy(resource, destination);
        } catch (IOException e) {
            e.printStackTrace();
            if (BuildConfig.DEBUG) Log.i("Save File", "Save Failed");
        }
    }

    private static void copy(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();
    }
}
