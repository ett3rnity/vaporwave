package com.fast_development_corp.vaporwavewallpers.utils;


import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.fast_development_corp.vaporwavewallpers.R;
import com.fast_development_corp.vaporwavewallpers.databinding.ItemVaporwavePhotoBinding;
import com.fast_development_corp.vaporwavewallpers.ui.SaveActivity;

import java.util.ArrayList;

public class VaporwaveAdapter extends RecyclerView.Adapter<VaporwaveAdapter.VaporwaveViewHolder> {
    private Context context;
    private ArrayList<String> linkList;
    private LayoutInflater layoutInflater;

    public VaporwaveAdapter(Context context) {
        this.context = context;
        linkList = LinkHolder.getLinkList();
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public VaporwaveViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VaporwaveViewHolder(DataBindingUtil.inflate(layoutInflater, R.layout.item_vaporwave_photo, parent, false).getRoot());
    }

    @Override
    public void onBindViewHolder(VaporwaveViewHolder holder, int position) {
        holder.binding.setIsImageLoaded(false);
        String link = linkList.get(holder.getAdapterPosition());
        RequestOptions options = new RequestOptions();
        options.centerCrop();
        Glide.with(context).setDefaultRequestOptions(options).load(link).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                holder.binding.setIsImageLoaded(true);
                return false;
            }
        }).into(holder.binding.ivPhoto);
        holder.itemView.setOnClickListener(v -> {
            System.out.println("Link: " + link);
            Intent intent = new Intent(context, SaveActivity.class);
            intent.putExtra("imageLink", link);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return linkList.size();
    }

    public class VaporwaveViewHolder extends RecyclerView.ViewHolder {
        ItemVaporwavePhotoBinding binding;

        public VaporwaveViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            binding.setIsImageLoaded(false);
        }
    }
}
