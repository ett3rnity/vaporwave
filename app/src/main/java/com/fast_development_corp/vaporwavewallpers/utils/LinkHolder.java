package com.fast_development_corp.vaporwavewallpers.utils;


import java.util.ArrayList;

public class LinkHolder {
    private static ArrayList<String> movieList;

    static {
        initList();
    }

    public static ArrayList<String> getLinkList() {
        return movieList;
    }

    public static void initList() {
        movieList = new ArrayList<>();

        movieList.add("https://i.imgur.com/0oiO4cy.png");

        movieList.add("https://i.imgur.com/x7t1VII.jpg");

        movieList.add("https://i.imgur.com/xuK3Ykv.jpg");

        movieList.add("https://i.imgur.com/lLxLmgJ.png");

        movieList.add("https://i.imgur.com/UXhhopJ.jpg");

        movieList.add("https://i.imgur.com/bvxKcRt.jpg");

        movieList.add("https://i.imgur.com/UiC8R9I.jpg");

        movieList.add("https://i.imgur.com/uZgpwRG.jpg");

        movieList.add("https://i.imgur.com/VwoK3R5.jpg");

        movieList.add("https://i.imgur.com/XTQEGYw.jpg");

        movieList.add("https://i.imgur.com/uPdunNk.jpg");

        movieList.add("https://i.imgur.com/Bgb4sDY.jpg");

        movieList.add("https://i.imgur.com/ZkBqIeQ.jpg");

        movieList.add("https://i.imgur.com/nz7oGcG.jpg");

        movieList.add("https://i.imgur.com/xd8D62S.jpg");

        movieList.add("https://i.imgur.com/ppv9Tbo.jpg");

        movieList.add("https://i.imgur.com/N19qmD9.jpg");

        movieList.add("https://i.imgur.com/ZqEo7rV.jpg");

        movieList.add("https://i.imgur.com/d0irU97.jpg");

        movieList.add("https://i.imgur.com/rPoYsAn.jpg");

        movieList.add("https://i.imgur.com/QHeQUPS.png");

        movieList.add("https://i.imgur.com/M01pZPd.png");

        movieList.add("https://i.imgur.com/hUYlhhi.png");

        movieList.add("https://i.imgur.com/KJZQYoa.jpg");

        movieList.add("https://i.imgur.com/W1jXWUM.jpg");

        movieList.add("https://i.imgur.com/ctgQ4Ng.jpg");

        movieList.add("https://i.imgur.com/e83XuCv.jpg");

        movieList.add("https://i.imgur.com/WbA5IcB.jpg");

        movieList.add("https://i.imgur.com/RQaKJAo.png");

        movieList.add("https://i.imgur.com/7PhXQWP.jpg");

        movieList.add("https://i.imgur.com/qiIwq3Q.jpg");

        movieList.add("https://i.imgur.com/2I8XIH4.jpg");

        movieList.add("https://i.imgur.com/VONVKGT.jpg");

        movieList.add("https://i.imgur.com/TsuE7uA.jpg");

        movieList.add("https://i.imgur.com/0WAzCG2.jpg");

        movieList.add("https://i.imgur.com/92PTApk.jpg");

        movieList.add("https://i.imgur.com/2DNZv4w.jpg");

        movieList.add("https://i.imgur.com/oBMzTWi.png");

        movieList.add("https://i.imgur.com/VVg7DrV.jpg");

        movieList.add("https://i.imgur.com/bhM2YeI.jpg");

        movieList.add("https://i.imgur.com/1HvXE7l.jpg");

        movieList.add("https://i.imgur.com/bjUb0eh.jpg");

        movieList.add("https://i.imgur.com/rWzrjDC.jpg");

        movieList.add("https://i.imgur.com/7a0OWIQ.jpg");

        movieList.add("https://i.imgur.com/IJU1rEW.jpg");

        movieList.add("https://i.imgur.com/R87IN7G.jpg");

        movieList.add("https://i.imgur.com/S8b67qR.jpg");

        movieList.add("https://i.imgur.com/MlevuXq.jpg");

        movieList.add("https://i.imgur.com/tMdS3Rk.jpg");

        movieList.add("https://i.imgur.com/ldzhfNZ.jpg");

        movieList.add("https://i.imgur.com/5fAfC4c.jpg");

        movieList.add("https://i.imgur.com/LzJdnDC.jpg");

        movieList.add("https://i.imgur.com/prpeuFQ.jpg");

        movieList.add("https://i.imgur.com/3oNymai.jpg");

        movieList.add("https://i.imgur.com/hUexpHP.jpg");

        movieList.add("https://i.imgur.com/w9IHaYt.jpg");

        movieList.add("https://i.imgur.com/LGoujhC.jpg");

        movieList.add("https://i.imgur.com/oAZCWzc.jpg");

        movieList.add("https://i.imgur.com/VkQXe6u.png");

        movieList.add("https://i.imgur.com/D0owHvE.jpg");

        movieList.add("https://i.imgur.com/g5vizWw.jpg");

        movieList.add("https://i.imgur.com/8JXD4SM.jpg");

        movieList.add("https://i.imgur.com/8LKyDwV.jpg");

        movieList.add("https://i.imgur.com/AusnyMQ.jpg");

        movieList.add("https://i.imgur.com/GLi7qmW.jpg");

        movieList.add("https://i.imgur.com/gh1JuY3.png");

        movieList.add("https://i.imgur.com/OzgLUfa.jpg");

        movieList.add("https://i.imgur.com/OMhW3BX.jpg");

        movieList.add("https://i.imgur.com/F6ore8b.jpg");

        movieList.add("https://i.imgur.com/BswkHAG.jpg");

        movieList.add("https://i.imgur.com/UxueqJn.jpg");

        movieList.add("https://i.imgur.com/zRxtYLp.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/25/c2/93/25c293860370b8590e9427f001e5b204.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/08/d8/06/08d806621a377af5495ac171f3897271.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/c8/1a/64/c81a6495cc5637d2763687c66a09e1ff.jpg");

        movieList.add("https://i.imgur.com/nIVvLTm.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/51/b0/d4/51b0d4692e60764ffbf4ec08168bd493.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/43/be/8c/43be8c5d35664a4e3c280c827faa9fd5.jpg");

        movieList.add("https://i.imgur.com/QkiWWfS.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/96/ec/b0/96ecb0b088d5385e2be879dbee4b9068.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/c0/d1/fe/c0d1fe33e5f25330801c6bef0bdcfffb.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/4b/24/41/4b244169a1c0399c22350093d1d0013d.jpg");

        movieList.add("https://i.imgur.com/PW3mhvu.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/01/6a/01/016a013e4eb1041740463f81e2b7ace2.jpg");

        movieList.add("https://i.imgur.com/s9AWg9m.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/d3/4b/69/d34b6919236996ebe04fa564d7fb3ec2.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/e9/20/54/e92054a07aba4d41c4c4698e6ee96570.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/35/ac/c2/35acc2c8712bcc2c5f46315135583329.jpg");

        movieList.add("https://i.imgur.com/AsMpI4W.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/a1/e6/52/a1e65226a7c86f74baa4688bd6300cff.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/de/a0/86/dea08640a2a96ab32d4ee5562f3e9cf7.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/fd/17/63/fd1763e532ad193a8c80c8dbfedb0c82.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/95/2b/87/952b87b8a137cef12bc66c710749e40a.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/f7/5b/30/f75b30921a8a57fcbe65829aebded366.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/e8/e1/92/e8e192acae0e8095955751c4526ae1ae.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/55/61/44/556144dc3efb3a0d35d823371bae16cf.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/d3/97/d2/d397d2121f9d1170c236acc1f6f4f792.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/17/c1/7a/17c17a52f00b85cf5bd90e55aab93d98.jpg");

        movieList.add("https://i.imgur.com/5MO0oPE.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/b1/92/72/b192721c2e780b3288e0ebfb65884352.jpg");

        movieList.add("https://i.imgur.com/FtLQmMi.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/1b/cc/25/1bcc256febe347dda8ed2005e0c98b9d.jpg");

        movieList.add("https://i.imgur.com/WQ8wa2B.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/8f/51/d3/8f51d35579d7841ac2b7cd1fead0be8a.jpg");

        movieList.add("https://i.imgur.com/egMt1Ke.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/79/cf/be/79cfbe20bac952c28dd1866474801518.jpg");

        movieList.add("https://i.imgur.com/bZ3mih7.jpg");

        movieList.add("http://i.imgur.com/PSrkZEn.png");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/76/65/09/766509c4fc36557a1cffde159f2df041.jpg");

        movieList.add("https://i.imgur.com/VrVjLtg.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/0e/6a/85/0e6a85db13b1bd4611b79e0ae6a57881.jpg");

        movieList.add("http://i.imgur.com/mjJxsyg.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/f6/a7/42/f6a742c225404d475df35ea1db4c84ca.jpg");

        movieList.add("https://i.imgur.com/wtiVQ4F.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/60/ff/e7/60ffe71addedda12c5fc4993681953e1.jpg");

        movieList.add("https://i.imgur.com/BdJdk0Z.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/c9/de/e1/c9dee1dab4128c57a98bf004ad2c6471.jpg");

        movieList.add("https://i.imgur.com/ktpx35m.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/6a/3f/51/6a3f510a69b084e34d02e9772554e3b2.jpg");

        movieList.add("https://i.imgur.com/i2LQ4AN.jpg");

        movieList.add("http://imgs.abduzeedo.com/files/wallpapers/wpw364/wp_iPhone.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/3c/e0/04/3ce00424064ace343983a33d60dec957.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/736x/82/47/7c/82477cf9ba460a09e111f043fabab3a2.jpg");

        movieList.add("http://i.imgur.com/xE6pnsc.jpg");

        movieList.add("http://pre01.deviantart.net/b278/th/pre/f/2016/024/2/f/future_tech__mobile__by_theflatrat-d9p8nz2.png");

        movieList.add("https://i.imgur.com/Cu351uR.jpg");

        movieList.add("https://i.redd.it/nzsswtss1uax.jpg");

        movieList.add("https://mfiles.alphacoders.com/616/616085.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/originals/77/e4/75/77e47530a95e16b2a1cc97fd3524bd75.jpg");

        movieList.add("https://i.imgur.com/S48fYmU.jpg");

        movieList.add("https://i.imgur.com/4oMMZuV.jpg");

        movieList.add("https://i.imgur.com/tqTdwQa.jpg");

        movieList.add("https://i.imgur.com/ESRrgjq.jpg");

        movieList.add("http://www.nmgncp.com/data/out/184/5198626-hotline-miami-iphone-wallpaper.png");

        movieList.add("https://i.redd.it/wstbzeoih0ux.jpg");

        movieList.add("https://i.imgur.com/PnMYRF4.png");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/18/67/89/1867896b75671dce151df792a4679219.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/originals/e3/62/b4/e362b496ecff5ea774ca72895d8e642b.png");

        movieList.add("http://imgur.com/86ZZJl3.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/bc/13/ff/bc13ffcb8669454b2ef5c6ae115300ae.jpg");

        movieList.add("https://i.imgur.com/apBGOvS.jpg");

        movieList.add("https://i.imgur.com/5dyvHW2.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/e6/b1/68/e6b1686533ab42def9a246df21288522.jpg");

        movieList.add("https://i.imgur.com/Murz5mu.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/31/b2/8d/31b28d514e8eef72a5819bbe2931a286.jpg");

        movieList.add("https://i.imgur.com/aruQ2R0.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/73/da/60/73da60a49591408cac22c596ba7cf460.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/44/6b/66/446b66b02d92d21e6431422f004e13ff.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/1d/75/0e/1d750ed71ca8429cc7d0ef24783f22d6.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/46/43/76/4643760a24a8a1bbf05a1b16283c566e.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/43/c4/2b/43c42be828c3947428640e6a8caf5eb1.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/f8/72/66/f87266c2310d351b5a1baca058e6881d.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/e6/0a/39/e60a39c06f56f3aedb61c793fedee995.jpg");

        movieList.add("https://i.imgur.com/HIMBAvN.jpg");

        movieList.add("https://i.imgur.com/HPZZHxM.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/e3/b0/9c/e3b09c2acfc4e53aa2a39126f68c8723.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/2d/25/75/2d2575ef486559ed5ad60655ef152c84.jpg");

        movieList.add("https://i.imgur.com/xDjAgOe.jpg");

        movieList.add("https://i.imgur.com/NXxiILY.jpg");

        movieList.add("https://i.imgur.com/nALodj2.jpg");

        movieList.add("https://i.imgur.com/2P6iVBh.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/89/be/8c/89be8c72a6cfb30e79080b4c07ff81f7.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/736x/a2/00/17/a20017c5e42fadb73d77f18f66c59bca.jpg");

        movieList.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQURxR2SOf-pGeyT3SFEo0Ph2TMS5qglepu4hDoW8Okj54k_oL7");

        movieList.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSdSMUqyhXdmnFDQcxhYC3dFqXM0oi_HHOqspTmtcennrdvJW5w");

        movieList.add("https://s-media-cache-ak0.pinimg.com/236x/47/99/a2/4799a27df246201bc1a013bc8f5dbb18.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/2e/17/fe/2e17fea520edee9870511dfdc8fa9f07.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/f1/bd/5e/f1bd5e0864ea34aba179c088109db489.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/47/b7/91/47b7910168527179f970880a54d229e6.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/f9/b2/a2/f9b2a2251e90cae913680f03782056f9.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/18/8e/1e/188e1e058ef344184349cb7535d51a32.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/a2/57/79/a25779a9b676613c41e10be978f4d13c.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/c5/ff/26/c5ff26f2fc96b0950c8a6c7ade2b264f.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/736x/a1/d1/6f/a1d16f5081ade19e485489bb6f0b09fd.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/736x/43/10/a9/4310a9351d85325364d9aec3a65fb6c0.jpg");

        movieList.add("https://i.imgur.com/KKwq7x7.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/bd/42/1d/bd421d6c7c7fb025023a271e5c0f8ee2.jpg");

        movieList.add("https://i.imgur.com/iEfsUsn.jpg");

        movieList.add("https://i.imgur.com/ZNqVMAt.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/de/0a/5a/de0a5ae434979f0a1a4864f07fae365e.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/c5/ba/d6/c5bad6fa6f6cd5eaf2d9c8cdcfdcf70e.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/03/8c/26/038c265464a8cb5665f4cfb6e411708d.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/59/b1/87/59b1879b2e643ef132121f8d9fba0153.jpg");

        movieList.add("https://i.imgur.com/zol5sg3.jpg");

        movieList.add("https://i.imgur.com/fiFWLhm.jpg");

        movieList.add("https://i.imgur.com/SrzB2J1.jpg");

        movieList.add("https://i.imgur.com/9cStUSE.jpg");

        movieList.add("https://i.imgur.com/qhZZ3UJ.jpg");

        movieList.add("https://i.imgur.com/iclM0RS.jpg");

        movieList.add("https://i.imgur.com/axIImC5.jpg");

        movieList.add("https://i.imgur.com/4VPHqb1.jpg");

        movieList.add("https://i.imgur.com/3BmEvU5.jpg");

        movieList.add("https://i.imgur.com/wkoiFb4.jpg");

        movieList.add("https://i.imgur.com/FhQ9SaB.jpg");

        movieList.add("https://i.imgur.com/6U04y8R.jpg");

        movieList.add("https://i.imgur.com/vqWSzcu.jpg");

        movieList.add("https://i.imgur.com/ZmTa3Er.jpg");

        movieList.add("https://i.imgur.com/8cVRWtl.jpg");

        movieList.add("https://i.imgur.com/T6YriW2.jpg");

        movieList.add("https://i.imgur.com/ZdqzdOY.jpg");

        movieList.add("https://i.imgur.com/9Yb0u7f.png");

        movieList.add("https://i.imgur.com/aYAGVyF.jpg");

        movieList.add("https://i.imgur.com/vl4QNaB.jpg");

        movieList.add("https://i.imgur.com/Q2noXrs.jpg");

        movieList.add("https://i.imgur.com/ok5Rj4t.jpg");

        movieList.add("https://i.imgur.com/B5qIDZE.jpg");

        movieList.add("https://i.imgur.com/CHY2SLM.jpg");

        movieList.add("https://i.imgur.com/v1ZRXcO.jpg");

        movieList.add("https://i.imgur.com/oRn8y9Q.jpg");

        movieList.add("https://i.imgur.com/B5qIDZE.jpg");

        movieList.add("https://i.imgur.com/DClQtQP.jpg");

        movieList.add("https://i.imgur.com/E04HpOv.jpg");

        movieList.add("https://i.imgur.com/bO8hqhg.jpg");

        movieList.add("https://i.imgur.com/6GBEHq3.jpg");

        movieList.add("https://i.imgur.com/11Hs2I3.jpg");

        movieList.add("https://i.imgur.com/qE9nPmF.jpg");

        movieList.add("https://i.imgur.com/DCghbsu.jpg");

        movieList.add("https://i.imgur.com/T3hIFiw.jpg");

        movieList.add("https://i.imgur.com/JRcsNXc.jpg");

        movieList.add("https://i.imgur.com/FxLWpmX.jpg");

        movieList.add("https://i.imgur.com/G6hHS6p.jpg");

        movieList.add("https://i.imgur.com/jyLa459.jpg");

        movieList.add("https://i.imgur.com/SJsTBf0.jpg");

        movieList.add("https://i.imgur.com/hX5tpbP.jpg");

        movieList.add("https://i.imgur.com/75MmixK.jpg");

        movieList.add("https://i.imgur.com/VURIkOs.jpg");

        movieList.add("https://i.imgur.com/AJu8MVS.jpg");

        movieList.add("https://i.imgur.com/uvYirC7.jpg");

        movieList.add("https://i.imgur.com/GwvsZZ8.jpg");

        movieList.add("https://i.imgur.com/EXG6Xvt.jpg");

        movieList.add("https://i.imgur.com/mh2iyq2.jpg");

        movieList.add("https://i.imgur.com/QPMqS0C.jpg");

        movieList.add("https://i.imgur.com/xe5czkw.jpg");

        movieList.add("https://i.imgur.com/gsxQ32e.jpg");

        movieList.add("https://i.imgur.com/LVPlkUw.jpg");

        movieList.add("https://i.imgur.com/aDYEueO.jpg");

        movieList.add("https://i.imgur.com/XkhVd5C.jpg");

        movieList.add("https://i.imgur.com/T8jTzD4.png");

        movieList.add("https://i.imgur.com/HvaslRI.jpg");

        movieList.add("https://i.imgur.com/I9Dqtkx.jpg");

        movieList.add("https://i.imgur.com/mrBBrnu.jpg");

        movieList.add("https://i.imgur.com/rXB9Cnd.jpg");

        movieList.add("https://i.imgur.com/c3QWMAe.jpg");

        movieList.add("https://i.imgur.com/kXuthCW.jpg");

        movieList.add("https://i.imgur.com/htljeiu.jpg");

        movieList.add("https://i.imgur.com/Y5e3Vbm.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/564x/90/b2/6a/90b26abf80823e74310b4ee6438bd1e5.jpg");

        movieList.add("https://s-media-cache-ak0.pinimg.com/originals/dd/d8/15/ddd815a1a1f5312925b48f90c15f890d.jpg");

        movieList.add("https://i.redd.it/uwf0rawg5oex.jpg");

        movieList.add("http://wallpapercave.com/wp/wp1895677.jpg");
    }
}
