package com.fast_development_corp.vaporwavewallpers.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;

import com.fast_development_corp.vaporwavewallpers.R;
import com.fast_development_corp.vaporwavewallpers.utils.VaporwaveAdapter;
import com.fast_development_corp.vaporwavewallpers.databinding.ActivityVaporwaveBinding;

public class VaporwaveActivity extends AppCompatActivity {
    private ActivityVaporwaveBinding binding;
    private VaporwaveAdapter adapter;
    private GridLayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_vaporwave);

        initVaporwaveImages();
    }

    private void initVaporwaveImages() {
        adapter = new VaporwaveAdapter(this);
        layoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        binding.rvImages.setAdapter(adapter);
        binding.rvImages.setLayoutManager(layoutManager);
    }
}
