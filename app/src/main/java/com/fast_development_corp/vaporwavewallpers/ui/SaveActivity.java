package com.fast_development_corp.vaporwavewallpers.ui;


import android.annotation.SuppressLint;
import android.app.WallpaperManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.fast_development_corp.vaporwavewallpers.R;
import com.fast_development_corp.vaporwavewallpers.databinding.ActivitySaveBinding;
import com.fast_development_corp.vaporwavewallpers.utils.FileUtils;
import com.fast_development_corp.vaporwavewallpers.utils.PermisisonUtils;

import java.io.File;
import java.io.IOException;

public class SaveActivity extends AppCompatActivity {
    private ActivitySaveBinding binding;
    private String imageLink;
    private boolean saveGranted = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_save);
        saveGranted = PermisisonUtils.checkPermissions(this);
        initReceive();

        initImage();
        initButtons();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Thanks, now you can save your image!", Toast.LENGTH_SHORT).show();
            saveGranted = true;
        } else {
            Toast.makeText(this, "Sorry, but this permission is needed to save images!", Toast.LENGTH_SHORT).show();
            saveGranted = false;
        }
    }


    private void initReceive() {
        Intent intent = getIntent();
        imageLink = intent.getStringExtra("imageLink");
    }

    @SuppressLint("CheckResult")
    private void initImage() {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.centerCrop();
        Glide.with(this).setDefaultRequestOptions(requestOptions).load(imageLink).into(binding.ivImage);
    }

    private void initButtons() {
        binding.tvDownload.setOnClickListener(v -> {
            if (saveGranted) {
                Glide.with(this).downloadOnly().load(imageLink).into(new SimpleTarget<File>() {
                    @Override
                    public void onResourceReady(@NonNull File resource, @Nullable Transition<? super File> transition) {
                        FileUtils.saveImageFile("testing", resource);
                        Toast.makeText(SaveActivity.this, "Successfully saved!", Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                PermisisonUtils.askPermissions(this);
            }
        });

        binding.tvSetWallpaper.setOnClickListener(v -> {
            binding.ivImage.buildDrawingCache();
            Bitmap bitmap = binding.ivImage.getDrawingCache();
            WallpaperManager wallpaperManager = WallpaperManager.getInstance(this);
            try {
                wallpaperManager.setBitmap(bitmap);
                Toast.makeText(this, "Check out new Wallpaper!", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(this, "An error occurred(((", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
